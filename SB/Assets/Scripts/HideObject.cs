﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObject : MonoBehaviour {

    private GameObject obj;
    private bool disable = true;

    private void Awake()
    {
        obj = gameObject;
        obj.SetActive(false);
    }

    public void Hide()
    {
        if (disable)
        {
            obj.SetActive(true);
            disable = false;
        }
        else
        {
            obj.SetActive(false);
            disable = true;
        }
    }
}
