﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Load : MonoBehaviour {

    private string savePath;

    BubbleCharacter bubbleCharacter;

    public Text desc;
    public new Text name;

    private void Awake()
    {
        savePath = Application.dataPath + "/Save.xml";
        bubbleCharacter = gameObject.GetComponent<BubbleCharacter>();
    }

    public void LoadData()
    {
        XDocument doc = XDocument.Load(savePath); // Загружаем файл

        foreach (XElement _name in doc.Root.Elements(Tags.Name))
        {
            bubbleCharacter.Name = _name.Value;
        }

        foreach (XElement _desc in doc.Root.Elements(Tags.Descrition))
        {
            bubbleCharacter.Description = _desc.Value;
        }

        foreach (XElement _id in doc.Root.Elements(Tags.Id))
        {
            bubbleCharacter.Id = Int32.Parse(_id.Value);
        }

        foreach (XElement _longitude in doc.Root.Elements(Tags.Location))
        {
            bubbleCharacter.Longitude = float.Parse(_longitude.Attribute(Tags.Longitude).Value);            
        }

        foreach (XElement _latitude in doc.Root.Elements(Tags.Latitude))
        {
            bubbleCharacter.Latitude = float.Parse(_latitude.Attribute(Tags.Latitude).Value);
        }
    }
}
