﻿using System.Xml.Linq;
using System.IO;
using UnityEngine;

public class Save : MonoBehaviour
{
    private string savePath;
    private XmlData xmlWriter;

    void Awake()
    {
        savePath = Application.dataPath + "/Save.xml";
        xmlWriter = gameObject.GetComponent<XmlData>();        
    }

    public void SaveData()
    {
        Debug.Log(savePath);
        XElement saveXmlDoc = xmlWriter.GetXmlData();

        if (!File.Exists(savePath))
        {
            XDocument _saveXmlDoc = new XDocument();

            _saveXmlDoc.Add(new XElement(Tags.RootDocTag));         // Добавляем корневой тэг

            File.WriteAllText(savePath, _saveXmlDoc.ToString());    // Записываем xml файл
        }
        else
        {
            XDocument _saveXmlDoc = XDocument.Load(savePath);       // Загрузка xml файла
            
            _saveXmlDoc.Root.Add(saveXmlDoc);                       //Добавляем элемент

            File.WriteAllText(savePath, _saveXmlDoc.ToString());    // Записываем xml файл
        }
    }

    //public void Load()
    //{
    //    XElement root = null;

    //    if (File.Exists(savePath))
    //    {
    //        root = XDocument.Parse(File.ReadAllText(savePath)).Element(rootElement);
    //    }
    //}
}


