﻿using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

public class XmlData : MonoBehaviour
{

    private BubbleCharacter bubbleCharacters;
    
    private void Awake()
    {
        
        //tags = gameObject.GetComponent<Tags>();
        bubbleCharacters = gameObject.GetComponent<BubbleCharacter>();
    }

    public XElement GetXmlData()
    {
        
        XAttribute _latitude =  new XAttribute(Tags.Latitude, bubbleCharacters.Latitude);
        XAttribute _longitude = new XAttribute(Tags.Longitude, bubbleCharacters.Longitude);
        XAttribute _id = new XAttribute(Tags.Id, bubbleCharacters.Id);

        List<XElement> xElements = new List<XElement>
        {
            new XElement(Tags.Id, bubbleCharacters.Id),
            new XElement(Tags.Name, bubbleCharacters.Name),
            new XElement(Tags.Descrition, bubbleCharacters.Description),
            new XElement(Tags.Location, _latitude, _longitude)
        };

        XElement _rootTag = new XElement(Tags.RootElementTag);

        xElements.ForEach(_xElement => _rootTag.Add(_xElement));

        return _rootTag;
    } 
}
