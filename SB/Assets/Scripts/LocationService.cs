﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LocationService : MonoBehaviour {
    
    public float Latitude { get; set; }
    public float Longitude { get; set; }

    //public Text text;

    public IEnumerator Start()
    {
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            print("On GPS");
            //text.text = "On GPS";
            Latitude = 56.438665F;
            Longitude = 85.122804F;            
            yield break;
        }
            

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            //text.text = "Timed out";
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            //text.text = "Unable to determine device location";
            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
            string a = "Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp;
            Latitude = Input.location.lastData.latitude;
            Longitude = Input.location.lastData.longitude;
            print("Access granted");
            //text.text = "Access granted ";
        }

        // Stop service if there is no need to query location updates continuously
        //Input.location.Stop();
    }
}
