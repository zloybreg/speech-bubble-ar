﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour {

    public static Singleton singleObj = null;

    private void Start()
    {
        if (singleObj == null)
        {
            singleObj = this;

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyObject(gameObject);
        }        
    }
}
