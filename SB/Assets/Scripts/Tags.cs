﻿public static class Tags
{
    public static string Latitude               { get; } = "Latitude";
    public static string Longitude              { get; } = "Longitude";
    public static string Descrition             { get; } = "Descrition";
    public static string Name                   { get; } = "Name";
    public static string Id                     { get; } = "Id";
    public static string RootElementTag         { get; } = "Bubble";
    public static string Location               { get; } = "Location";
    public static string RootDocTag             { get; } = "SpeechBubble";
}
