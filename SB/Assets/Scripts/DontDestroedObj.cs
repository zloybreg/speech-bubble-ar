﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroedObj : MonoBehaviour
{
    
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
