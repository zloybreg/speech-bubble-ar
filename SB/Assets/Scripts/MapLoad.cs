﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapLoad : MonoBehaviour {

    const string key = "SG4UnAHwsrGfYp8iPSpnLW4gNQiUOXRX";
    Renderer mapRenderer;
    Vector2 bubblePos; // = new Vector2(56.498792F, 84.953459F); //Lattitude, Longitude
    LocationService location;
    Text text;
    LoadPrefab prefab;

    int mapZoom = 14;
    int mapWidth = 1280;
    int mapHeight = 1280;
    string mapType = "map";
    string mapUrl;

    private void Awake()
    {
        mapRenderer = GameObject.Find("Map").GetComponent<Renderer>();
        text = GameObject.Find("MapLoadBar").GetComponent<Text>();
        location = gameObject.GetComponent<LocationService>();
        prefab = gameObject.GetComponent<LoadPrefab>();
    }

    private void Start()
    {
        StartCoroutine(StartLoadMap());        
    }

    public IEnumerator StartLoadMap()
    {
        yield return StartCoroutine(location.Start());

        print("Wait Local Servicr Start");
        text.text = "Wait Local Service Start";

        bubblePos = new Vector2(location.Latitude, location.Longitude);
        
        text.text = "Current pos" + bubblePos;
        
        mapUrl = "https://open.mapquestapi.com/staticmap/v5/map?"
                + "key=" + key
                + "&center=" + bubblePos.x.ToString(System.Globalization.CultureInfo.InvariantCulture) 
                + "," 
                + bubblePos.y.ToString(System.Globalization.CultureInfo.InvariantCulture)
                + "&size=" + mapWidth + "," + mapHeight
                + "&zoom=" + mapZoom
                + "&type=" + mapType;

        print(mapUrl);

        StartCoroutine(LoadMapImage());
    }

    private IEnumerator LoadMapImage()
    {
        WWW www = new WWW(mapUrl);

        while (!www.isDone)
        {           
            text.text = "progress " + Math.Round(www.progress * 100) + "%";            
            yield return null;
        }

        if (www.error == null)
        {
            Debug.Log("Map ready!");
            text.text = "Map ready! " + bubblePos.ToString();
            yield return new WaitForSeconds(0.5f);

            mapRenderer.material.mainTexture = null;

            Texture2D tmpTexture;
            tmpTexture = new Texture2D(mapWidth, mapHeight, TextureFormat.RGB24, false);

            mapRenderer.material.mainTexture = tmpTexture;

            www.LoadImageIntoTexture(tmpTexture);
        }
        else
        {
            Debug.Log("Map error: " + www.error);
            text.text = "Map error: " + www.error;
            yield return new WaitForSeconds(1);

            mapRenderer.material.mainTexture = null;
        }

        mapRenderer.enabled = true;

        StartCoroutine(prefab.InstantiateObject());
    }
}
