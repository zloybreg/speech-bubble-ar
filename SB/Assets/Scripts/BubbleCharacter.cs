﻿using UnityEngine;
using UnityEngine.UI;

public class BubbleCharacter : MonoBehaviour {

    private LocationService locationService;

    //public InputField bubbleName;
    //public InputField bubbleDesc;
    //public Text bubbleLocation;

    public string Name { get; set; }
    public string Description { get; set; }
    public int Id { get; set; } = 1;
    public float Latitude { get; set; }
    public float Longitude { get; set; }

    private void Awake()
    {
        locationService = gameObject.GetComponent<LocationService>();
    }

    private void Update()
    {
        Latitude = locationService.Latitude;
        Longitude = locationService.Longitude;

        //Name = bubbleName.text;
        //Description = bubbleDesc.text;
        //bubbleLocation.text = Longitude.ToString() + " " + Latitude.ToString();
    }
}
